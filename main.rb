require 'rubygems'
require 'gosu'


## Game Classes
load 'bullets.rb'
load 'shooter.rb'


require_relative 'boxes'


class  Game < Gosu::Window

  include Boxes
  
  #Eventually want to shorten this
  def initialize
    @xmax = 640
    @ymax = 480
    @caption = "Box Invader"
    
    reset_game
    @gamestate = :start
    
    super(@xmax, @ymax, false)
    self.caption = @caption

    @shooter = Shooter.new(0, @ymax - 70)
    load_images

    #This is required to setup box creation so class methods knows boundries
    Box.boundry(@xmax, @ymax - 80 - @shooter.height)

  end

  def reset_game
    @score = 0
    @highestscore = 0
    @level = 0
    @bullets = []  #bullets on screen
    @boxes = []    #color boxes on screen
    @startingtime = 0
    reset_boxes
  end
    
    

  def load_images

    @background_image = Gosu::Image.new(self, "images/background.png", true)
    @startscreen = Gosu::Image.new(self, "images/startscreen.png", true)
    @gameover = Gosu::Image.new(self, "images/gameover.png", true)
    @highscorebox = Gosu::Font.new(self, "bold arial", 50)
    @levelbox = Gosu::Font.new(self, "bold arial", 100)
    @bullet = Gosu::Image.new(self, "images/bullet.png", true)
    @shooter.image = Gosu::Image.new(self, "images/shooter.png", true)
    @scorebox = Gosu::Font.new(self, "bold arial", 50)
    
  end
 
  def update

    case @gamestate
      when :playing then playing_update
      when :gameover then gameover_update
      when :start then start_update
    end
    
  end

  def draw
    
    case @gamestate
      when :playing then playing_draw
      when :start then start_draw
      when :gameover then gameover_draw
    end
    
  end

  def start_update

    if button_down? Gosu::KbSpace
      reset_game
      @gamestate = :playing
      @startingtime = Time.now
    end
    
  end

  def start_draw
    @background_image.draw(0, 0, 0)
    draw_in_center(@startscreen)    
  end
  

  def gameover_update
    @gamestate = :start if button_down? Gosu::KbReturn
  end

 
  def gameover_draw
    @background_image.draw(0, 0, 0)
    draw_in_center(@gameover)
    @highscorebox.draw(@highestscore.to_s, @xmax / 2.05, @ymax / 2.05, 4)
    @levelbox.draw(@level.to_s, @xmax * 0.467, @ymax * 0.65, 4)
  end

  
  def playing_update

    #See if bullets collide with boxes
    killcolor
    #checks if reached highscore - used for end of game
    highest_score?
    
    #Checks if player has any buttons down and reacts
    do_player_move

    update_boxes
    #Spawns color boxes to attack
    create_boxes(@level) if new_level?
    
    @gamestate = :gameover if game_over?

  end

  def playing_draw
    
    @background_image.draw(0, 0, 0)
    @shooter.image.draw( @shooter.x, @shooter.y, 1)

    draw_bullets

    draw_boxes

    #checks arrays for boxes and bullets out of window range.
    #Deletes them if they are out
    update_image_arrays

    display_score

    @gamestate = :gameover if game_over?
    
  end

  #Draws an image centered in the bakcground
  def draw_in_center(image)
    
    #Center based on startscreen and background
    x = @background_image.width / 2 - image.width / 2
    y = @background_image.height / 2 - image.height / 2
    image.draw(x, y, 1)
    
  end
  
  def draw_bullets

    #draw each bullet that is in the array
    @bullets.each do |b|      
      @bullet.draw(b.x, b.y, 1)
      b.update
    end
  end

  def draw_boxes
    
    boxes_to_draw.each do |b|
      draw_quad(b.x, b.y, b.color,
                b.x + b.width, b.y, b.color,
                b.x + b.width, b.y + b.height, b.color,
                b.x, b.y + b.height, b.color)
      end
  end
                
  

  def game_over?
    @score < 0 || box_hit_player?
  end

  
  def box_hit_player?
    boxes_to_draw.any? {|b| b.collided_with? @shooter}
  end     
  
  
  def highest_score?
    @highestscore = @score if @highestscore < @score
  end

  def do_player_move

    @shooter.update
    @shooter.move_left(0) if button_down? Gosu::KbLeft
    @shooter.move_right(@xmax - @shooter.width) if button_down? Gosu::KbRight
    shoot_bullet if button_down? Gosu::KbSpace
      
  end

  def shoot_bullet

    if @shooter.can_shoot?
      @bullets << Bullet.new( @shooter.x + (@shooter.width / 2), @shooter.y,
                              @bullet.height, @bullet.width)
      @shooter.add_delay(20)
    end
    
  end
  
  

  def new_level?

    if @level == 0
      @levelstart = Time.now
      @level += 1

    elsif boxes_to_draw == []
      @levelstart = Time.now
      @level += 1  
    else
      false
    end
      
  end
    

 

  # Needs to be refactored
  def killcolor
    @bullets.each do |bullet|
      boxes_to_draw.each do |box|
          if box.collided_with? bullet
            @bullets.delete(bullet)
            Box.list.delete(box)
            @score += 1
            highest_score?
          end
       
      end
    end
    
  end

 
  def update_image_arrays
    @bullets.delete_if{ |b| b.y <= - @bullet.height}

    Box.list.each do |b|
      if b.y >= (@ymax + b.height)
        Box.list.delete(b)
        @score -= 1
      end
    end
  
  end

  def display_score
    
    @scorebox.draw(@score.to_s, @xmax / 2, 
                   @ymax - @scorebox.height / 1.5, 3, 1, 1, Gosu::Color::BLACK)
  end

end



window = Game.new
window.show

