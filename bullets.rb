require_relative 'collide'

class Bullet
  attr_accessor :x, :y, :height, :width, :speed

  include Collide

  def initialize(x, y, height, width)
    @x = x
    @y = y
    @height = height
    @width = width
    @speed = 8
  end

  def update
    @y = @y - @speed
  end
  
  
end

