require 'rubygems'
require 'gosu'

require_relative 'collide'


module Boxes

  ## Only the module functions are needed for boxes
  
  def box_boundries(xmax, ymax)
    Box.boundry(xmax, ymax)
  end
  
  #changes the locations/size of boxes
  def update_boxes
    Box.to_draw.each {|b| b.update}
  end

  #returns an array of boxes to draw
  def boxes_to_draw
    Box.to_draw
  end

  def delete_box(box)
    Box.delete_box(box)
  end

  #creates boxes for that given level
  def create_boxes(level)

    Box.creation(level)
   
  end

  def reset_boxes
    Box.reset
  end

  
class Box

  include Collide
  
  attr_reader :x, :y, :height, :width, :color, :create_at

  Colors = {
    :red => Gosu::Color::RED,
    :blue => Gosu::Color::BLUE,
    :green => Gosu::Color::GREEN,
    :aqua => Gosu::Color::AQUA,
    :yellow => Gosu::Color::YELLOW,
    :fuchsia => Gosu::Color::FUCHSIA,
    :cyan => Gosu::Color::CYAN
  }
  
  #It is required to call this before using this class
  #It will set the boundry that boxes can spawn at.
  def Box.boundry(xmax, ymax)
    @@xmax = xmax
    @@ymax = ymax
    @@boxes = []
  end

  def Box.list=(list)
    @@boxes = list
  end

  def Box.delete_box(box)
    @@boxes.delete(box)
  end

  def Box.reset
    @@boxes = []
  end

  def Box.list
    @@boxes
  end
    
  def initialize(type, delay = 0)

    @type = type
    @color = Colors[type]
    @create_at = Time.now + delay

    method = "create_" + @type.to_s
    
    send(method)
    
  end

  def Box.to_draw
    @@boxes.select{|b| b.time_has_come?}
  end

  def update

    if time_has_come?
      method = "update_" + @type.to_s
      send(method)
    end
    
  end

  def Box.delay_all(boxes, delay)

    boxes.each{ |b| @create_at += delay}
  end

  

  #Randomly starts right above x axis
  def create_red
    
    @width = @@xmax / 20
    @height = @@ymax / 20

    @y = 0 - height

    @x = rand( (@@xmax - @width) - 0) + 0

    @speed = (rand(30) + 1) * 0.1

  end

  #Red boxes just move down
  def update_red
    @y += @speed
  end

  #Blue boxes start randomly in any location
  def create_blue

    @width = @@xmax / 30
    @height = @@ymax / 10

    @y = rand( (@@ymax - @height) - 0) + 0
    @x = rand( (@@xmax - @width) - 0) + 0
   
    @timer = rand(300-200) + 200

  end

  #After time runs out they drop down fast
  def update_blue

    @timer -= 1 if @timer >= 1

    @y += 12 if @timer <= 0
    
  end

  def create_green

    @height = 1
    @width = 1
    
    @y = rand(@@ymax - @height)
    @x = rand(@@xmax - @width)

  end

  def update_green

    @y -= 0.25
    @x -= 0.25
    @width += 0.5
    @height += 0.5
    
  end

  def create_yellow
    @height = 10
    @width = 40

    @ampt = rand(15) + 1
    @speed = (rand(2) + 1) * 0.2

    @y = 0
    @x = @@xmax / 2

    if rand(2) == 0
      @x += rand(100)
    else
      @x -= rand(100)
    end

    @xaxis = @x

    @angle = 3.14
    @ycount = 0

  end

  def update_yellow

    @ycount += @speed
    @angle += 0.1
    @y = @ampt * 5 * Math.sin(@angle) + @ycount - @ampt
    @x = @ampt * 5 * Math.cos(@angle) + @xaxis

    
  end
  
  def create_aqua

    @height = 20
    @width = 20

    @ampt = rand(15) + 1
    @speed = rand(3) + 1

    @y = 0
    @angle = 3.14
    @x = @@xmax / 2

    if rand(2) == 0
      @x += rand(200) - @ampt
    else
      @x -= rand(200) - @ampt 
    end

  end

  #Will move like a sign wave
  def update_aqua

    @y += @speed
    @angle += 0.1

    @x += @ampt * Math.sin(@angle)

  end

  def update_cyan
  end

  


  def Box.creation(level)

    
    boxes = []
    
    

    if special_level?(level) 
      return special_level
    end

    level.times {|b| boxes.push(Box.new(:red,b))}
  
    if level % 2 == 0
      (rand(level) + 1).times {|b| boxes.push(Box.new(:blue,b))}
    end

    if level % 3 == 0
      (rand(level) + 1).times {|b| boxes.push(Box.new(:green,b))}
    end

    if level % 4 == 0
      (rand(level) + 1).times {|b| boxes.push(Box.new(:aqua, b))}
    end

    if level % 5 == 0
      (rand(level) + 1).times {|b| boxes.push(Box.new(:yellow, b))}
    end

    @@boxes += boxes
    
  end

  
  def Box.special_level? level

    if rand(100) <= 20 and level >= 6
      true
    else
      false
    end
    
  end

  def Box.special_level

    boxes = []

    special = [ [50, :red],
                [15, :green],
                [30, :blue],
                [20, :aqua],
                [20, :yellow], ]

    n = rand(special.size)

    special[n][0].times {|b| boxes.push(Box.new(special[n][1], b * 0.1))}

    boxes
  end


  def time_has_come?
    @create_at <= Time.now
  end


  
end

    
end
